#!/bin/bash

_exit1() { echo "$1">&2 ; exit 1 ; } ;
_exit2() { echo "$1">&2 ; exit 2 ; } ;
_exit3() { echo "$1">&2 ; exit 2 ; } ;

for baseprog in git cpulimit curl mkdir python3 pip3 ;do 
which "$baseprog" >/dev/null || _exit3 "missing $baseprog binary"
done

### BASEPATH=/migrate_in/ ;
### WPBULLETPATH=/usr/src/antivir/wpbullet/ ;
### WPCLIPATH=${WPCLIPATH}
### REPORTSFOLDER=/avscan/
### SCANUSER=www-data

source .env 

timestamp() { date -u +%F_%H.%M.%S ; } ;


STARTTIME=$(timestamp)
test -d /tmp/.avscan/${REPORTSFOLDER} || ( mkdir -p /tmp/.avscan/${REPORTSFOLDER} ;chown -R ${SCANUSER} /tmp/.avscan/${REPORTSFOLDER} )
test -d /tmp/.avscan/${REPORTSFOLDER} || exit 23

echo $(timestamp)" :: WEBROOT SCAN START"

### WP CLI
test -f ${WPCLIPATH}/wp || (
    echo "GETTING WP CLI"
    mkdir -p ${WPCLIPATH}/
    curl -L https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > ${WPCLIPATH}/wp
    )


#WP CLI NEEDS TO RUN IN CONTEXT WITH DATABASE CONNECTION FOR CORE CHECKS
test -f ${WPCLIPATH}/wp && (
    echo "Updating WP CLI"
    curl -L https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > /tmp/wpcli$STARTTIME
    diff ${WPCLIPATH}/wp /tmp/wpcli$STARTTIME || mv /tmp/wpcli$STARTTIME ${WPCLIPATH}/wp
    )

###


### WP Bullet
echo "WP Bullet"
test -f /${WPBULLETPATH}/wpbullet.py || git clone https://github.com/webarx-security/wpbullet /${WPBULLETPATH}/
test -f /${WPBULLETPATH}/wpbullet.py && ( cd /${WPBULLETPATH}/ ; git pull ;pip3 install -r requirements.txt 2>&1 |grep -v "Requirement already satisfied" )

chown ${SCANUSER}:${SCANUSER} /tmp/.avscan/${REPORTSFOLDER}


# find ${BASEPATH} -maxdepth 3 -name "wp-config.php"|sed 's/\/wp-config.php//g'|sed 's/'${BASEPATH//\//\\\/}'//g'|grep -v ^$|wc -l|grep ^0$ && _exit1 "NO WORDPRESS FOUND"

echo "scanning for  wordpress sites"

find ${BASEPATH} -maxdepth 3 -name "wp-config.php"|sed 's/\/wp-config.php//g'|sed 's/'${BASEPATH//\//\\\/}'//g'|grep -v ^$|while read domain;do ( echo -n $(timestamp)" checking WordPress ${domain}   ";
    cd /tmp/.avscan/${REPORTSFOLDER};test -d ${domain} || (mkdir ${domain} ;chown -R ${SCANUSER} /tmp/.avscan/${REPORTSFOLDER} );
    test -d /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports || ( mkdir /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports;chown ${SCANUSER} /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports )

    echo -n ":#WP-CLI#CORE"
    ##CORE UPDATES
    buffer=$(docker exec -t ${domain} wp --path=${BASEPATH}/${domain} core check-update  2>&1 );

    echo "$buffer" | grep  -q "No such container" || (
        echo "$buffer" | grep -q "^Success:" ||  ( ( (echo "NEED CORE UPDATE:";echo "$buffer") | tee -a /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/wpcli.log ) )
       )
        
    echo "$buffer" | grep  -q "No such container" && (
        (echo "::CANNOT TEST CORE UPDATES:";echo "$buffer";echo ) | tee -a /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/wpcli.log  |tr -d '\n'    
       )


    
    echo -n "#PLUG-UPD"
    ## PLUGIN UPDATES
    buffer=$(su -s /bin/bash -c "php ${WPCLIPATH}/wp --path=${BASEPATH}/${domain} plugin update --dry-run --all --format=csv 2>/dev/null|grep -v ^name,status" ${SCANUSER} ) ;
    updates_avail=no
    echo "$buffer"|wc -l |grep ^0$ && updates_avail=yes
    echo "$updates_avail"|grep -q "^yes$" && ( ( echo "PLUGIN UPDATES AVAILABLE:";echo "$buffer"|sed 's/,/ | /g;s/\(^\|$\)/|/g')  | tee -a /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/wpcli.log )

    echo -n "#CHKSUM"
    ##VERIFY CHECKSUMS
    buffer=$(su -s /bin/bash -c "php ${WPCLIPATH}/wp --path=${BASEPATH}/${domain} core verify-checksums  2>&1" ${SCANUSER} );
    echo "$buffer" | grep -q "WordPress installation verifies against checksums" ||  (( echo "CHECKSUM ERRORS:";echo;echo "$buffer" ) | tee -a /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/wpcli.log )
    
    echo -n "#PLUG-CHKSUM"
    ##VERIFY CHECKUM PLUGINS 
    buffer=$(su -s /bin/bash -c "php ${WPCLIPATH}/wp --path=${BASEPATH}/${domain} plugin verify-checksums  --all 2>&1" ${SCANUSER} ) 
    echo "$buffer"|grep ^Error|grep -q skipped && ( ( echo "CHECKSUMS UNAVAILABLE:";echo "$buffer"|grep  -e skipping -e skipped  ) | tee -a /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/wpcli.log )
    echo "$buffer"|grep -q -e "File is missing" -e "Checksum does not match" -e "File was added" && ( ( echo "PLUGIN CHECKSUM ERRORS:";echo;echo "$buffer"|grep -e "File is missing" -e "Checksum does not match" -e "File was added" ) | tee -a /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/wpcli.log )

    echo -n ":wpbullet:"
    export PYTHONIOENCODING=utf-8 ;
    cd  /tmp/.avscan/${REPORTSFOLDER}/${domain}/
    su -s /bin/bash -c "cd /tmp/.avscan/${REPORTSFOLDER}/${domain}/;PYTHONIOENCODING=utf-8 python3 /${WPBULLETPATH}/wpbullet.py  --path ${BASEPATH}/${domain} --report reports 2>&1 " ${SCANUSER} &> /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/WPBULLET.log
    echo -n "clamscan:"
    ps -ALFc|grep freshclam|grep -v grep |grep bin/freshclam -q || freshclam &> /dev/null;
    cpulimit --quiet --foreground -m -l 150 -- clamscan --exclude='\.(mov|avi|mp4|MP4|MOV|AVI|JPG|JPEG|PNG|GIF|WEBP|jpg|jpeg|png|gif|webp)$' -r -i ${BASEPATH}/${domain}  &> /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/CLAM.log 
    cd /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports;   
    sed 's/\r//g' WPBULLET.log -i ;
    #### no CR#################
    ( echo "REPORT FOR WORDPRES $domain at "$(timestamp) ;
    echo ;

    test -f /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/CLAM.log &&  ( grep -q "^Infected files: 0$" /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/CLAM.log || ( 
       echo "#####CLAMAV####";
       cat /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/CLAM.log;
       echo "####";echo ) );
    
    
    echo "###WPBULLET";
    grep ^Checked /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/WPBULLET.log |sed 's/\r/\n/g;s/Checked files/\nChecked files/g'|grep "^Checked files" |tail -n1 ;cat /tmp/.avscan/${REPORTSFOLDER}/${domain}/reports/WPBULLET.log |sed 's/\r/\n/g' |grep -v "Checked files"|sed 's/'${BASEPATH//\//\\\/}'//g'| tr -cd '\11\12\15\40-\176' |sed 's/(0x(B/|/g;s/(0l(B\[32m/\n###########################################\n\n/g;s/\[0m(0q.\+/\n___\n/g;s/(0tq.\+//g;s/(0l(B\[31m/\n/g;s/\[39m//g;s/\[31m//g'|grep -v "^(0mq")|sed 's/$/ENDOFLINE/g'|tr -d '\n'|sed 's/|ENDOFLINEENDOFLINE|/|ENDOFLINE|/g'|sed 's/ENDOFLINE/\n/g' |txt2html --tables|while read line;do
        #########↑ only last checked file count ######################################↑↑ remove basepath ↑↑ ######## ↑↑ no unprintables ########### ↑replace table format  ↑replace table format  ↑replace table format  ↑replace table format  ↑replace table format  ↑replace table format  ↑replace table format ######### ↑↑ remove accidential newlines between pipe , so txt2html finds our table ↑↑ ########             ### html report 
        case ${line} in
            \|*\| ) echo  "${line}"|sed 's/^|/<tr><td>/g;s/|$/<\/td><\/tr>/g;s/ | /<\/td><td>/g' ;;
            ########REPLACE non-recognized pipes to form HTML Table
            *) echo "${line}" ;;
        esac;
   done|sed 's/$/ENDOFLINE/g'|tr -d '\n'|sed 's/<\/tr>ENDOFLINE<\/pre>/<\/tr><\/table>ENDOFLINE<\/pre>/g;s/<pre>ENDOFLINE<tr>/<pre><table border="1" summary="">ENDOFLINE<tr>/g'|sed 's/ENDOFLINE/\n/g' |sed 's/( '"'"'<\/td><td>/ | /g'> /tmp/.avscan/${REPORTSFOLDER}/${domain}/report.html ) ;

echo; done


##########################################################  ↑↑ BUILD HTML TABLE from unrecognized pipe signs ↑↑ ####################################################################                          ## replace accidential pipe replace


##### COLLECT AND SAVE
echo $(timestamp)"WAITING FOR SCANNERS TO COMPLETE ";
wait;

cd /tmp/.avscan/${REPORTSFOLDER} && tar cvzf /tmp/avscanreport.${STARTTIME}.tgz .

test -f /tmp/avscanreport.${STARTTIME}.tgz || echo "TAR FILE NOT GENERATED"
test -f /tmp/avscanreport.${STARTTIME}.tgz && ( echo "TAR FILE  GENERATED, sending reports"
mylogdest=/tmp/avscanreport.${STARTTIME}.tgz

WPSUMMARY="";

for wpclilog in $(find "/tmp/.avscan/${REPORTSFOLDER}" -name "wpcli.log" );do 
    grep -e "PLUGIN UPDATES AVAILABLE" -e "NEED CORE UPDATE" -e "CHECKSUMS UNAVAILABLE" -e "CHECKSUM ERRORS" -e "CANNOT TEST CORE UPDATES"  $wpclilog -q && WPSUMMARY="${WPSUMMARY} ""${wpclilog//$BASEPATH/g}"" MYEOL" ;
    for finding in "PLUGIN UPDATES AVAILABLE" "NEED CORE UPDATE" "CHECKSUMS UNAVAILABLE" "CHECKSUM ERRORS" "CANNOT TEST CORE UPDATES";do
        grep -q "${finding}"  "${wpclilog}" && WPSUMMARY="${WPSUMMARY}""${finding}""MYEOL" ;
    done ;
done ;

WPSUMMARY=$(echo "${WPSUMMARY}"|sed 's/MYEOL/\n/g'|sed 's/\/tmp\/.avscan\/'${REPORTSFOLDER//\//\\\/}'/=>/g' );



### SEND TELEGRAM REPORT #### 
TELEGRAM_CONFIG=false
test -f /etc/telegram-notify.pam.conf    && TGCONFIG_FILE=/etc/telegram-notify.pam.conf
test -f /etc/telegram-notify.backup.conf && TGCONFIG_FILE=/etc/telegram-notify.backup.conf
test -f /etc/telegram-notify.backup.conf && TGCONFIG_FILE=/etc/telegram-notify.avscan.conf

grep -v ^api-key=$ ${TGCONFIG_FILE} |grep -v -q ^api-key=bot && TELEGRAM_CONFIG=OK
#echo $TELEGRAM_CONFIG
#echo $TGCONFIG_FILE

which telegram-notify &>/dev/null && (
    #echo TG_NOTIFY_FOUND
    echo ${TELEGRAM_CONFIG} |grep -q ^OK$ && (
    echo "sending"
    MSGTXT=$(date)" Webroot Vulnerabilites and Updates Scan finished , log attached"
    MSGTXT=$(echo "${MSGTXT}" ;echo; echo "${WPSUMMARY}")    
    
    
    #grep -e Error -e ERROR -e error -e FAILED -e failed -e Failed ${mylogdest} -q && (echo "(Errors)""$MSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent --document /tmp/   _${startdate}.log --warning --text -)  || ( echo "$MSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent --document /tmp/docker2restic_${startdate}.log --text - )
    (echo "(AV-SCAN)""$MSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent --document /tmp/avscanreport.${STARTTIME}.tgz --warning --text -)  || ( echo "$MSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent --document /tmp/avscanreport.${STARTTIME}.tgz --text - )
     ) )
rm /tmp/avscanreport.${STARTTIME}.tgz

rm -rf /tmp/.avscan/${REPORTSFOLDER}
)

