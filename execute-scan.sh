#!/bin/bash

_exit1() { echo "$1">&2 ; exit 1 ; } ;
_exit2() { echo "$1">&2 ; exit 2 ; } ;
_exit3() { echo "$1">&2 ; exit 2 ; } ;

_dedup() { awk '!x[$0]++' ; } ;

baseprogs="git cpulimit curl mkdir python3 pip3 txt2html jq tr"
for baseprog in $baseprogs ;do
which "$baseprog" >/dev/null || _exit3 "missing $baseprog binary , please install $baseprogs"
done

test -e /tmp/.avscan_wpbullet/wpbullet.py || git clone https://github.com/webarx-security/wpbullet /tmp/.avscan_wpbullet/ 2>&1
test -e /tmp/.avscan_wpbullet/wpbullet.py && { cd /tmp/.avscan_wpbullet/ ; git pull 2>&1  ; } ;

### BASEPATH=/migrate_in/ ;
WPBULLETPATH=/tmp/.avscan_wpbullet ;
### WPCLIPATH=${WPCLIPATH}
REPORTSFOLDER=/avscan/
SCANUSER=www-data

#source .env

timestamp() { date -u +%F_%H.%M.%S ; } ;

_docker_detect_mounts() {
target="$1";inspectsnip=$(docker inspect $target);
echo "$inspectsnip" |jq  '.[]| .HostConfig | .Binds'|sed 's/^ \+//g'
echo -n ; } ;

_docker_detect_mounts_docroot() {
target="$1";inspectsnip=$(docker inspect $target);
#echo "$inspectsnip" |jq  '.[]| .HostConfig | .Binds'
echo "$inspectsnip" |jq  '.[]| .HostConfig | .Binds'|grep -e docroot: -e /var/www |grep -v -e /var/www/typo3_src -e /var/www/.ssh|sed 's/^ \+//g'
echo -n ; } ;

_docker_resolve_mount_folders() {
while read folder;do
  if  [[ "${folder}" =~ ^\"/ ]] ; then
    echo "${folder}" ;
  else
    echo "${folder}" | sed 's/^"/\/var\/lib\/docker\/volumes\//g;s/:/\/_data:/g' ;
  fi ;
done ; } ;
###

_scan_wordp_container() {
target="$1";
folders=$(_docker_detect_mounts_docroot "$target" | _docker_resolve_mount_folders |sed 's/"//g'|cut -d: -f1 );

mkdir -p /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/

##UPDATE THE CONTAINER FIRST
echo $(timestamp)"#CLAMAV"
cpulimit --quiet --foreground -m -l 80 -- clamscan --exclude="\.toolkit/sql\.php$" --exclude='\.(mov|avi|mp4|MP4|MOV|AVI|JPG|JPEG|PNG|GIF|WEBP|jpg|jpeg|png|gif|webp)$' -r -i ${folders} 2>&1 |grep -v -e "pdf: Porcupine.Malware.41917.UNOFFICIAL FOUND" -e .toolkit/sql.php: &> /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/CLAM.log

DOCROOT=UNDETECTED;for curdoc in  $folders ;do test -e ${curdoc}/wp-config.php && DOCROOT=${curdoc};test -e ${curdoc}/html/wp-config.php && DOCROOT=${curdoc}/html/;done


    echo -n $(timestamp)"#PLUG-UPD-CHECK"
    ## PLUGIN UPDATES
    buffer=$(docker exec -it ${target} su -s /bin/bash -c "wp --path=/var/www/html plugin update --dry-run --all --format=csv 2>/dev/null|grep -v ^name,status" ${SCANUSER} ) ;
    updates_avail=no
    echo "$buffer"|wc -l |grep ^0$ && updates_avail=yes
    echo "$updates_avail"|grep -q "^yes$" && ( ( echo "PLUGIN UPDATES AVAILABLE(will be triggered in a few moments):";echo "$buffer"|sed 's/,/ | /g;s/\(^\|$\)/|/g') | tee -a /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/wpcli.log )
    echo

    echo -n $(timestamp)"#UPDATE"
    docker exec -it ${target} su -s /bin/bash -c "test -f /wordpress-update-wwwdata.sh && bash /wordpress-update-wwwdata.sh 2>&1  ;test -f /wordpress-update-wwwdata.sh || wp core update 2>&1 ; wp plugin update --all 2>&1 " www-data 2>&1 | _dedup &> /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/wp-updates.log
    echo
    echo -n $(timestamp)"#CHKSUM"
    ##VERIFY CHECKSUMS
    buffer=$(docker exec -it ${target} su -s /bin/bash -c "wp --path=/var/www/html core verify-checksums  2>&1" ${SCANUSER} |grep -v -e "t verify against checksum: wp-config-sample.php" -e "File should not exist: wp-admin/.htaccess" );
    echo "$buffer" | grep -q -e "t verify against checksum" ||  (( echo "CHECKSUM ERRORS:";echo;echo "$buffer" ) | tee -a /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/wpcli.log )
    echo
    echo -n $(timestamp)"#PLUG-CHKSUM"
    ##VERIFY CHECKUM PLUGINS
    buffer=$(docker exec -it ${target} su -s /bin/bash -c "wp --path=/var/www/html plugin verify-checksums  --all 2>&1" ${SCANUSER} )
    echo "$buffer"|grep ^Error|grep -q skipped && ( ( echo "CHECKSUMS UNAVAILABLE:";echo "$buffer"|grep  -e skipping -e skipped  ) | tee -a /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/wpcli.log )  ## "
    echo "$buffer"|grep -q -e "File is missing" -e "Checksum does not match" -e "File was added" && ( ( echo "PLUGIN CHECKSUM ERRORS:";echo;echo "$buffer"|grep -e "File is missing" -e "Checksum does not match" -e "File was added" ) | tee -a /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/wpcli.log )
    echo

    echo -n $(timestamp)":wpbullet:"
    export PYTHONIOENCODING=utf-8 ;
    #mkdir -p /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/wp-bullet /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/wp-bullet
    #cd  /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/wp-bullet

    mkdir -p /${WPBULLETPATH}/reports
    chown ${SCANUSER}   /${WPBULLETPATH}/reports
####
    su -s /bin/bash -c "cd   /${WPBULLETPATH}/ ;PYTHONIOENCODING=utf-8 python3 /${WPBULLETPATH}/wpbullet.py  --path ${DOCROOT} --report reports 2>&1 " ${SCANUSER} &> /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/WPBULLET.log
    mv  /${WPBULLETPATH}/reports /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/wpbullet-full
    sed 's/\r//g'  /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/WPBULLET.log -i;
    #### no CR#################
    echo
## wp bullet to HTML
cat /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/WPBULLET.log |sed 's/\r/\n/g' |grep -v "Checked files"|sed 's/'${DOCROOT//\//\\\/}'//g'| tr -cd '\11\12\15\40-\176' |sed 's/(0x(B/|/g;s/(0l(B\[32m/\n###########################################\n\n/g;s/\[0m(0q.\+/\n___\n/g;s/(0tq.\+//g;s/(0l(B\[31m/\n/g;s/\[39m//g;s/\[31m//g'|grep -v "^(0mq"|sed 's/$/ENDOFLINE/g'|tr -d '\n'|sed 's/|ENDOFLINEENDOFLINE|/|ENDOFLINE|/g'|sed 's/ENDOFLINE/\n/g' |txt2html --tables|while read line;do
###########################################↑ only last checked file count ######################################↑↑ remove basepath ↑↑ ######## ↑↑ no unprintables ########### ↑replace table format  ↑replace table format  ↑replace table format  ↑replace table format  ↑replace table format  ↑replace table format  ↑replace table format ######### ↑↑ remove accidential newlines between pipe , so txt2html finds our table ↑↑ ########             ### html report
case ${line} in
    \|*\| ) echo  "${line}"|sed 's/^|/<tr><td>/g;s/|$/<\/td><\/tr>/g;s/ | /<\/td><td>/g' ;;
    ########REPLACE non-recognized pipes to form HTML Table
    *) echo "${line}" ;;
esac;
 done|sed 's/$/ENDOFLINE/g'|tr -d '\n'|sed 's/<\/tr>ENDOFLINE<\/pre>/<\/tr><\/table>ENDOFLINE<\/pre>/g;s/<pre>ENDOFLINE<tr>/<pre><table border="1" summary="">ENDOFLINE<tr>/g'|sed 's/ENDOFLINE/\n/g' |sed 's/( '"'"'<\/td><td>/ | /g'> /tmp/.avscan/${REPORTSFOLDER}/${target}/report.html
 ##########################################################  ↑↑ BUILD HTML TABLE from unrecognized pipe signs ↑↑ ####################################################################                          ## replace accidential pipe replace
## summary
  ( echo "REPORT FOR WORDPRESS $domain at "$(timestamp) ;
    echo ;
    test -f /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/CLAM.log &&  ( grep -q "^Infected files: 0$" /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/CLAM.log || (
      echo "#####CLAMAV####";
      cat /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/CLAM.log;
      echo "####";echo ) );
      echo "###WPBULLET";
      grep ^Checked /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/WPBULLET.log |sed 's/\r/\n/g;s/Checked files/\nChecked files/g'|grep "^Checked files" |tail -n1 ;

  ) |tee -a /tmp/.avscan/${REPORTSFOLDER}/${target}/report.summary.log|grep -e iles -e Infected


echo -n ; } ;


_scan_typo3_container() {
target="$1";
folders=$(_docker_detect_mounts_docroot "$target" | _docker_resolve_mount_folders |sed 's/"//g'|cut -d: -f1 );
mkdir -p /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/
cpulimit --quiet --foreground -m -l 80 -- clamscan --exclude="\.toolkit/sql\.php$" --exclude='\.(mov|avi|mp4|MP4|MOV|AVI|JPG|JPEG|PNG|GIF|WEBP|jpg|jpeg|png|gif|webp)$' -r -i ${folders} 2>&1 |grep -v -e "pdf: Porcupine.Malware.41917.UNOFFICIAL FOUND" -e .toolkit/sql.php: &> /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/CLAM.log
docker exec -it $target su -s /bin/bash -c "cd /tmp/;git clone https://github.com/Tuurlijk/typo3scan.git /tmp/typo3scan ;cd /tmp/typo3scan;php typo3scan scan /var/www/html/ ;rm -rf /tmp/typo3scan" www-data &> /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/typo3scan.log

echo -n ; } ;

_scan_gener_container() {
target="$1";
folders=$(_docker_detect_mounts_docroot "$target" | _docker_resolve_mount_folders |sed 's/"//g'|cut -d: -f1 );
mkdir -p /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/
cpulimit --quiet --foreground -m -l 80 -- clamscan --exclude="\.toolkit/sql\.php$" --exclude='\.(mov|avi|mp4|MP4|MOV|AVI|JPG|JPEG|PNG|GIF|WEBP|jpg|jpeg|png|gif|webp)$' -r -i ${folders} 2>&1 |grep -v -e "pdf: Porcupine.Malware.41917.UNOFFICIAL FOUND" -e .toolkit/sql.php: &> /tmp/.avscan/${REPORTSFOLDER}/${target}/reports/CLAM.log

echo -n ; } ;

####

STARTTIME=$(timestamp)
test -d /tmp/.avscan/${REPORTSFOLDER} || ( mkdir -p /tmp/.avscan/${REPORTSFOLDER} ;chown -R ${SCANUSER} /tmp/.avscan/${REPORTSFOLDER} )
test -d /tmp/.avscan/${REPORTSFOLDER} || exit 23

echo $(timestamp)" :: WEBROOT SCAN START"

#wordpress_containers=$(docker ps -a --format '{{.Names}}'|grep -e _wordpress -e _wp-fpm|sed 's/_.\+//g')
allcontainers=$(docker ps -a --format '{{.Names}}')
scancontainers=$(echo "$allcontainers"|grep -v -e "^nginx"|grep -v -e ^typo3.local$ -e _redirwww$ -e _database$ -e _memcached$ -e _webserver$ -e _wp-fpm$ -e ^0000-redirect $(cat /etc/avscan.docker.exclude |sed 's/^/-e /g'  )  |sort -u)
## update clam sigs if daaemon is no running
ps -ALFc|grep freshclam|grep -v grep |grep bin/freshclam -q || freshclam &> /dev/null;


chown ${SCANUSER}:${SCANUSER} /tmp/.avscan/${REPORTSFOLDER}

echo $(timestamp)" : scanning sites"


for currentcont in  $scancontainers ;do
    CONTAINER_TYPE=GENERIC ;
    inspectsnip=$(docker inspect $currentcont);
    typesearch=$( echo "$inspectsnip" |jq -c '.[]| .HostConfig , .Args' ) ;
    echo "$typesearch" |grep -q -e wordpress && CONTAINER_TYPE=WORDPRESS  ;
    echo "$typesearch" |grep typo3 -q && CONTAINER_TYPE=TYPO3 ;
    echo "$typesearch" |grep socat |grep -q -e "tcp4-listen:" && CONTAINER_TYPE=SKIP ;
    echo $(timestamp)" : "$CONTAINER_TYPE $currentcont ;
    case $CONTAINER_TYPE in
      WORDPRESS) _scan_wordp_container "$currentcont" ;;
      TYPO3)     _scan_typo3_container "$currentcont"  ;;
      GENERIC)   _scan_gener_container "$currentcont"  ;;
   esac;
done

##### COLLECT AND SAVE

##remove clam logs with no hits
find "/tmp/.avscan/${REPORTSFOLDER}"  -name CLAM.log|while read curlog ;do grep "^Infected files: 0$" "$curlog" -q  && rm "$curlog" ;done

##BUILD WORDPRESS summary
WPSUMMARY="";

for wpclilog in $(find "/tmp/.avscan/${REPORTSFOLDER}" -name "wpcli.log" );do
  ## remove cli logs with only wp-config-sampe.php failling
  sed 's/.\+WordPress installation doesn'"'"'t verify against checksums.//g;s/.\+File doesn.t verify against checksum: wp-config-sample.php//g;s/WordPress installation verifies against checksums.//g;s/^ \+$//g;s/^\t\+//g' "${wpclilog}" |tr -dc '[[:print:]]'|grep -v -e "CHECKSUM ERRORS:" -e  ^$ |wc -l |grep -q ^0$ && rm "${wpclilog}"
    grep -e "PLUGIN UPDATES AVAILABLE" -e "NEED CORE UPDATE" -e "CHECKSUMS UNAVAILABLE" -e "CHECKSUM ERRORS" -e "CANNOT TEST CORE UPDATES"  $wpclilog -q && WPSUMMARY="${WPSUMMARY} ""${wpclilog//$REPORTSFOLDER/g}"" MYEOL" ;
    for finding in "PLUGIN UPDATES AVAILABLE" "NEED CORE UPDATE" "CHECKSUMS UNAVAILABLE" "CHECKSUM ERRORS" "CANNOT TEST CORE UPDATES";do
        grep -q "${finding}"  "${wpclilog}" && WPSUMMARY="${WPSUMMARY}""${finding}""MYEOL" ;
    done ;
done ;

WPSUMMARY=$(echo "${WPSUMMARY}"|sed 's/MYEOL/\n/g'|sed 's/\/tmp\/.avscan\/'${REPORTSFOLDER//\//\\\/}'/=>/g;s/\/tmp\/.avscan\//=>/g' );

## BUILD TYPO3 summary
TYPOSUMMARY="";
for typoclilog in $(find "/tmp/.avscan/${REPORTSFOLDER}" -name "wpcli.log" );do
FINDINGS=NO
cat "${typoclilog}"|grep "(weak)"   |wc -l |grep -q ^0$  || FINDINGS=YES
cat "${typoclilog}"|grep "(strong)" |wc -l |grep -q ^0$  || FINDINGS=YES

if [ "$FINDINGS" = "YES" ];then
 TYPOSUMMARY="${TYPOSUMMARY}"$(echo ${wpclilog}: STRONG: $(cat "${typoclilog}"|grep "(strong)"   |wc -l )  WEAK: $(cat "${typoclilog}"|grep "(weak)"   |wc -l ) )"MYEOL"
fi

done

TYPOSUMMARY=$(echo "${TYPOSUMMARY}"|sed 's/MYEOL/\n/g'|sed 's/\/tmp\/.avscan\/'${REPORTSFOLDER//\//\\\/}'/=>/g;s/\/tmp\/.avscan\//=>/g' );

##BUILD CLAMAV SUMMARY
CLAMSUMMARY=$(echo "CLAMAV RESULTS:" ;echo;
              find /tmp/.avscan/ -name CLAM.log |while read clamlog;do grep Infect ${clamlog}|grep -q "Infected files: 0$" || { echo $clamlog ; grep "Infected files:" ${clamlog} ; };
              done |sed 's/\/tmp\/.avscan\/'${REPORTSFOLDER//\//\\\/}'/=>/g;s/\/tmp\/.avscan\//=>/g;s/reports\//g;s/CLAM\.log//g')

find  /tmp/.avscan/${REPORTSFOLDER}/ -type d -empty -delete
cd /tmp/.avscan/${REPORTSFOLDER}/ && tar cvzf /tmp/avscanreport.${STARTTIME}.tgz .

test -f /tmp/avscanreport.${STARTTIME}.tgz || echo "TAR FILE NOT GENERATED"
test -f /tmp/avscanreport.${STARTTIME}.tgz && ( echo "TAR FILE  GENERATED, sending reports"
mylogdest=/tmp/avscanreport.${STARTTIME}.tgz

### SEND TELEGRAM REPORT ####
TELEGRAM_CONFIG=false
test -f /etc/telegram-notify.pam.conf    && TGCONFIG_FILE=/etc/telegram-notify.pam.conf
test -f /etc/telegram-notify.backup.conf && TGCONFIG_FILE=/etc/telegram-notify.backup.conf
test -f /etc/telegram-notify.backup.conf && TGCONFIG_FILE=/etc/telegram-notify.avscan.conf

grep -v ^api-key=$ ${TGCONFIG_FILE} |grep -v -q ^api-key=bot && TELEGRAM_CONFIG=OK
#echo $TELEGRAM_CONFIG
#echo $TGCONFIG_FILE

which telegram-notify &>/dev/null && (
    #echo TG_NOTIFY_FOUND
    echo ${TELEGRAM_CONFIG} |grep -q ^OK$ && (
    echo "sending"
    MSGTXT=$(date)" Webroot Vulnerabilites and Updates Scan finished , log attached"
    MSGTXT=$(echo "${MSGTXT}" ;echo; echo "${WPSUMMARY}";echo;echo "${TYPOSUMMARY}";echo;echo "${CLAMSUMMARY}")
    #grep -e Error -e ERROR -e error -e FAILED -e failed -e Failed ${mylogdest} -q && (echo "(Errors)""$MSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent --document /tmp/   _${startdate}.log --warning --text -)  || ( echo "$MSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent --document /tmp/docker2restic_${startdate}.log --text - )
    (echo "(AV-SCAN)""$MSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent --document /tmp/avscanreport.${STARTTIME}.tgz --warning --text -)
     ) ## grep TELEGRAM_CONFIG=OK
    ) # which telegram-notify
rm /tmp/avscanreport.${STARTTIME}.tgz

rm -rf /tmp/.avscan/${REPORTSFOLDER}/ ||true
) # test -f /tmp/avscanreport.${STARTTIME}.tgz
